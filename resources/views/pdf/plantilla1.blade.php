<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<!-- 
$url_contenido
$titulo_descripcion
$descripcion
$contratista
$titulo
$tipo_documento
$lugar
$n_obra
$obra
$codigo_cumento
$escala
$revision
$n_hoja

$margen_pagina
 -->

<body>
	<div class="main">
		<table border="1" cellpadding="5" cellspacing="0" id="sheet0" class="sheet0 gridlines">
			<thead class="bt-white">
				<tr class="row0" colspan="12">
					<th class="col0 b-none"></th>
					<th class="col1 b-none"></th>
					<th class="col2 b-none"></th>
					<th class="col3 b-none"></th>
					<th class="col4 b-none"></th>
					<th class="col5 b-none"></th>
					<th class="col6 b-none"></th>
					<th class="col7 b-none"></th>
					<th class="col8 b-none"></th>
					<th class="col9 b-none"></th>
					<th class="col10 b-none"></th>
					<th class="col11 b-none"></th>
				</tr>
			</thead>
			<tbody>
				<tr class="row0" rowspan="12">
					<td class="column0 style3 null style5" colspan="12">
						@if (isset($url_contenido))
						<img src="{!!$url_contenido!!}" alt="">
						@endif
					</td>
				</tr>
				<tr class="row41">
					<td class="column0 style10 null style12 inline f-8" colspan="4">
						CONTRATISTA:
					</td>
					<td class="column4 style10 null style12" colspan="8">
						{!!isset($titulo_descripcion)?$titulo_descripcion:''!!}
					</td>
				</tr>
				<tr class="row42">
					<td class="column0 style3 null style7" colspan="4" rowspan="2">
						{!!isset($contratista)?$contratista:''!!}
					</td>
					<td class="column4 style3 null style5" colspan="8">
						{!!isset($descripcion)?$descripcion:''!!}
					</td>
				</tr>
				<tr class="row44">
					<td class="column4 style3 null style7 inline f-8" colspan="1" rowspan="2">
						TITULO:
					</td>
					<td class="column6 style3 null style7" colspan="7" rowspan="2">
						{!!isset($titulo)?$titulo:''!!}
					</td>
				</tr>
				<tr class="row46">
					<td class="column0 style3 null style9 f-7" colspan="4" rowspan="2">
						ESTE DOCUMENTO ES PROPIEDAD DE XXXX QUIEN SALVAGUARDA SUS DERECHOS CONFORME A LAS PREVISIONES DE LA LEY.
					</td>
				</tr>
				<tr class="row47">
					<td class="column4 style10 null style12 inline f-8" colspan="1">
						TIPO DE DOC:
					</td>
					<td class="column6 style10 null style12" colspan="7">
						{!!isset($tipo_documento)?$tipo_documento:''!!}
					</td>
				</tr>
				<tr class="row48">
					<td class="column0 style3 null style7 inline f-8" colspan="4" rowspan="7">
						CLIENTE:
					</td>
					<td class="column4 style3 null style7 inline f-8" colspan="1" rowspan="2">
						LUGAR:
					</td>
					<td class="column6 style3 null style7" colspan="6" rowspan="2">
						{!!isset($lugar)?$lugar:''!!}
					</td>
					<td class="column11 style16 null inline f-8">
						N. DE OBRA:
					</td>
				</tr>
				<tr class="row49">
					<td class="column11 style13 null style14" rowspan="2">
						{!!isset($n_obra)?$n_obra:''!!}
					</td>
				</tr>
				<tr class="row51">
					<td class="column4 style3 null style5 inline f-8" colspan="1">
						OBRA:
					</td>
					<td class="column5 style16 null style5" colspan="6">
						{!!isset($obra)?$obra:''!!}
					</td>
				</tr>
				<tr class="row54">
					<td class="column4 style10 null style12 inline f-8" colspan="6">
						CODIGO DE DOCUMENTO:
					</td>
					<td class="column10 style16 null inline f-8">
						ESCALA:
					</td>
					<td class="column11 style16 null inline f-8">
						REVISION:
					</td>
				</tr>
				<tr class="row55">
					<td class="column4 style3 null style9" colspan="6" rowspan="3">
						{!!isset($codigo_cumento)?$codigo_cumento:''!!}
					</td>
					<td class="column10 style16 null">
						{!!isset($escala)?$escala:''!!}
					</td>
					<td class="column11 style13 null style15" rowspan="3">
						{!!isset($revision)?$revision:''!!}
					</td>
				</tr>
				<tr class="row56">
					<td class="column10 style16 null inline f-8">
						HOJA N.:
					</td>
				</tr>
				<tr class="row57">
					<td class="column10 style16 null">
						{!!isset($n_hoja)?$n_hoja:''!!}
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>

<style type="text/css">
	body {}

	.main {
		padding: <?= isset($margen_pagina) ? $margen_pagina : '2cm'; ?>;
		background-color: white;
	}

	table {
		width: 100%;
	}

	tr {}

	td {
		padding: 5px;
	}

	th,
	td {
		width: 8.33%;
		vertical-align: initial;
	}


	.inline {
		white-space: nowrap;
	}

	.f-10 {
		font-size: 1em;
	}

	.f-9 {
		font-size: 0.9em;
	}

	.f-8 {
		font-size: 0.8em;
	}

	.f-7 {
		font-size: 0.7em;
	}

	.f-6 {
		font-size: 0.6em;
	}

	.d-none {
		display: none;
	}

	.opacity-0 {
		opacity: 0;
	}

	.px-0 {
		padding-left: 0;
		padding-right: 0;
	}

	.py-0 {
		padding-top: 0;
		padding-bottom: 0;
	}

	.mx-auto {
		margin-left: auto;
		margin-right: auto;
	}

	.my-auto {
		margin-top: auto;
		margin-bottom: auto;
	}

	.bx-white {
		border-left: solid white !important;
		border-right: solid white !important;
	}

	.bt-white {
		border-top: solid white !important;
	}

	.b-none {
		border: none !important;
	}

	.b-solid {
		border: solid 1px;
	}

	.bt-solid {
		border-top: solid 1px;
	}

	.br-solid {
		border-right: solid 1px;
	}

	.bb-solid {
		border-bottom: solid 1px;
	}

	.bl-solid {
		border-left: solid 1px;
	}

	.by-solid {
		border-top: solid 1px;
		border-bottom: solid 1px;
	}

	.bx-solid {
		border-right: solid 1px;
		border-left: solid 1px;
	}

	.h-100 {
		height: 100%;
	}

	.h-50 {
		height: 50%;
	}

	.h-30 {
		height: 33.33%;
	}

	.w-100 {
		width: 100%;
	}

	.w-50 {
		width: 50%;
	}

	.w-30 {
		width: 33.33%;
	}
</style>

</html>