<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<!-- 
$url_contenido
$titulo_descripcion
$n_revision
$fecha_vigencia

$margen_pagina
 -->

<body>
	<div class="main">
		<table border="0" cellpadding="5" cellspacing="0" id="sheet0" class="sheet0 gridlines">
			<thead class="bt-white">
				<tr class="row0" colspan="12">
					<th class="col0 b-none"></th>
				</tr>
			</thead>
			<tbody>
				<tr class="row0" rowspan="12">
					<td class="column0 style3 null style5 p-5" colspan="12">
						@if (isset($url_contenido))
						<img src="{!!$url_contenido!!}" alt="">
						@endif
					</td>
				</tr>
				<tr class="row41">
					<td class="column0 style10 null style12 p-5" colspan="1">
						{!!isset($titulo_descripcion)?$titulo_descripcion:''!!}
					</td>
				</tr>
				<tr class="row41">
					<td class="column4 style10 null style12 p-5" colspan="1">
						Revision N.: {!!isset($n_revision)?$n_revision:''!!}
					</td>
				</tr>
				<tr class="row41">
					<td class="column4 style10 null style12 p-5" colspan="1">
						Fecha de vigencia: {!!isset($fecha_vigencia)?$fecha_vigencia:''!!}
					</td>
				</tr>

			</tbody>
		</table>
	</div>
</body>

<style type="text/css">
	body {}

	.main {
		padding: <?= isset($margen_pagina) ? $margen_pagina : '2cm'; ?>;
		background-color: white;
	}

	table {
		width: 100%;
	}

	tr {}

	td {
		padding: 5px;
	}

	th,
	td {
		width: 8.33%;
		vertical-align: initial;
		text-align: center;
	}


	.inline {
		white-space: nowrap;
	}

	.f-10 {
		font-size: 1em;
	}

	.f-9 {
		font-size: 0.9em;
	}

	.f-8 {
		font-size: 0.8em;
	}

	.f-7 {
		font-size: 0.7em;
	}

	.f-6 {
		font-size: 0.6em;
	}

	.d-none {
		display: none;
	}

	.opacity-0 {
		opacity: 0;
	}

	.p-5 {
		padding: 5em !important;
	}
	.px-0 {
		padding-left: 0;
		padding-right: 0;
	}

	.py-0 {
		padding-top: 0;
		padding-bottom: 0;
	}

	.mx-auto {
		margin-left: auto;
		margin-right: auto;
	}

	.my-auto {
		margin-top: auto;
		margin-bottom: auto;
	}

	.bx-white {
		border-left: solid white !important;
		border-right: solid white !important;
	}

	.bt-white {
		border-top: solid white !important;
	}

	.b-none {
		border: none !important;
	}

	.b-solid {
		border: solid 1px;
	}

	.bt-solid {
		border-top: solid 1px;
	}

	.br-solid {
		border-right: solid 1px;
	}

	.bb-solid {
		border-bottom: solid 1px;
	}

	.bl-solid {
		border-left: solid 1px;
	}

	.by-solid {
		border-top: solid 1px;
		border-bottom: solid 1px;
	}

	.bx-solid {
		border-right: solid 1px;
		border-left: solid 1px;
	}

	.h-100 {
		height: 100%;
	}

	.h-50 {
		height: 50%;
	}

	.h-30 {
		height: 33.33%;
	}

	.w-100 {
		width: 100%;
	}

	.w-50 {
		width: 50%;
	}

	.w-30 {
		width: 33.33%;
	}
</style>

</html>