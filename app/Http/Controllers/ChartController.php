<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function index()
    {
        $pdf = \PDF::loadView('pdf.plantilla2');
        
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);

        $pdf->setPaper('letter'); # 'letter','a4',etc...
        $pdf->setOrientation('portrait'); # 'landscape','portrait' 
        return $pdf->download('Documento_'.date('d-m-Y').'.pdf');
    }
}
